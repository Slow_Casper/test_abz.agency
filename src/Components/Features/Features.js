import { Box, Button, Container, Typography } from '@mui/material'
import React from 'react'
import { background, container, description } from './styles';
import { handleScrollTo } from '../../utils/scrollTo';

const Features = () => {
  return (
    <Box
        sx={background}
    >
        <Container sx={container}>
            <Typography
                variant='h1'
                sx={{
                    textAlign: 'center',
                    color: 'text.secondary',
                }}
            >
                Test assignment for front-end developer
            </Typography>
            <Typography 
                variant='body1' 
                sx={description}
            >
                What defines a good front-end developer is one that has skilled knowledge of HTML, CSS, JS with a vast understanding of User design thinking as they'll be building web interfaces with accessibility in mind. They should also be excited to learn, as the world of Front-End Development keeps evolving.
            </Typography>
            <Button onClick={() => handleScrollTo('postRequest')}>Sign up</Button>
        </Container>
    </Box>
  );
};

export default Features