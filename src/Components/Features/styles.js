export const background = {
    backgroundImage: 'url(./features.jpeg)',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    marginTop: '13px'
};

export const container = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: {
        mobile: '40px',
        tablet: '89px',
        desktop: '164px',
    },
    paddingBottom: {
        mobile: '71px',
        tablet: '88px',
        desktop: '163px',
    },
};

export const description = {
    textAlign: 'center',
    marginTop: '21px',
    marginBottom: '32px',
    color: 'text.secondary',
};
