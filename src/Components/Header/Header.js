import { Box, Button, CardMedia, Container, Typography, useMediaQuery } from '@mui/material';
import React from 'react';
import { btnContainer, flexCenter, logo } from './styles';
import { handleScrollTo } from '../../utils/scrollTo';

const Header = () => {
  const isLgTablet = useMediaQuery('(min-width: 481px)');

  return (
    <Container>
        <Box sx={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-between'
        }}>
            <Box
                sx={{
                    ...flexCenter,
                    columnGap: 1,
                }}
            >
                <CardMedia
                    component='img'
                    image='/logo.svg'
                    sx={logo}
                />
                {isLgTablet && <Typography variant='body1'>TESTTASK</Typography>}
            </Box>
            <Box
                sx={btnContainer}
            >
                <Button onClick={() => handleScrollTo('getRequest')}>Users</Button>
                <Button onClick={() => handleScrollTo('postRequest')}>Sign up</Button>
            </Box>
        </Box>
    </Container>
  );
};

export default Header