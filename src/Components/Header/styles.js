export const flexCenter = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
};

export const logo = {
    width: '38px',
    height: '26px',
};

export const btnContainer = {
    display: 'flex',
    columnGap: '10px'
};
