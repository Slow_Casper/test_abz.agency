import { Box, Button, Container, FormControl, FormControlLabel, FormLabel, Radio, RadioGroup, TextField, Typography } from '@mui/material'
import { Field, Form, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import validationSchema from './validationSchema';
import axios from 'axios';

import styles from './styledInput.module.scss';
import { container, formContainer, input } from './styles'
import { useDispatch } from 'react-redux';
import { fetchUsers, resetUsers } from '../../redux/slices/usersSlice';
import { handleScrollTo } from '../../utils/scrollTo';

const PostRequest = () => {
    // const [imageUrl, setImageUrl] = useState('');
    const [image, setImage] = useState('');
    const [radioButtons, setRadioButtons] = useState('');

    const dispatch = useDispatch();

    const initialValues = {
        name: '',
        email: '',
        phone: '',
        position_id: 1,
    };

    const fetchradio = async () => {
        await axios('https://frontend-test-assignment-api.abz.agency/api/v1/positions').then(res => {
            const { positions } = res.data;
            setRadioButtons(positions);
        });
    };

    useEffect(() => {
        fetchradio()
    }, []);

    // const cloudName = process.env.REACT_APP_CLOUD_NAME;
    // const uploadPreset = process.env.REACT_APP_UPLOAD_PRESET;
    // const uwConfig = {
    //   cloudName,
    //   uploadPreset,
    //   folder: `TestABZ.Agency`,
    // };
  
    // const handleOpenWidget = () => {
    //   const myWidget = cloudinary.createUploadWidget(
    //     uwConfig,
    //     (error, result) => {
    //       if (!error && result && result.event === 'success') {
    //         setImageUrl(result.info.secure_url);
    //       }
    //     },
    //   );
    //   myWidget.open();
    // };

    const handleSubmit = async (values, actions) => {
        const formData = new FormData(); 
        for(const [key, value] of Object.entries(values)) {
            formData.append(key, value);
        };
        formData.append('photo', image);
        const { token } = await axios('https://frontend-test-assignment-api.abz.agency/api/v1/token').then(res => res.data);     
        if (token){
            await axios.post('https://frontend-test-assignment-api.abz.agency/api/v1/users', formData, {headers: { 'Token': token }});
            dispatch(resetUsers())
            dispatch(fetchUsers('https://frontend-test-assignment-api.abz.agency/api/v1/users?page=1&count=6'))
            actions.resetForm({
                values: {
                    name: '',
                    email: '',
                    phone: '',
                    position_id: 1,
                },
            });
            handleScrollTo('getRequest');
        };
    };

    return (
        <Container id='postRequest' sx={container}>
            <Typography variant='h1'>Working with POST request</Typography>
            <Formik
                initialValues={initialValues}
                onSubmit={handleSubmit}
                validationSchema={validationSchema}
            >
                {({ isValid }) => (
                    <Form>
                        <Box sx={formContainer}>
                            <Field name='name'>
                                {({ field, form }) => (
                                    <Box sx={input} >
                                        <TextField {...field} label='Your name' variant='outlined' />
                                        {form.touched.name && (
                                            <Typography variant="caption" color="error">
                                                {form.errors.name}
                                            </Typography>
                                        )}
                                    </Box>
                                )}
                            </Field>
                            <Field name='email'>
                                {({ field, form }) => (
                                    <Box sx={input}>
                                        <TextField {...field} label='Email' variant='outlined' />
                                        {form.touched.email && (
                                            <Typography variant="caption" color="error">
                                                {form.errors.email}
                                            </Typography>
                                        )}
                                    </Box>
                                    
                                )}
                            </Field>
                            <Field name='phone'>
                            {({ field, form }) => (
                                    <Box sx={input}>
                                        <TextField {...field} label='Phone' variant='outlined' />
                                        {form.errors.phone && form.touched.phone && (
                                            <Typography variant="caption" color="error">
                                                {form.errors.phone}
                                            </Typography>
                                        )}
                                    </Box>
                                    
                                )}
                            </Field>
                            <FormControl component='fieldset'>
                                <FormLabel component='legend' id='position'>Select your position</FormLabel>
                                <Field name='position_id'>
                                    {({ field }) => (
                                        <RadioGroup
                                            aria-labelledby="demo-radio-buttons-group-label"
                                            {...field}
                                        >
                                            {radioButtons && radioButtons.map((el) => (
                                                <FormControlLabel key={el.id} type='number' value={el.id} control={<Radio/>} label={el.name} />
                                            ))}
                                        </RadioGroup>
                                    )}
                                </Field>
                            </FormControl>
                            <input
                                name='photo'
                                id='file'
                                type='file'
                                accept='.jpg, .jpeg'
                                aria-label='Upload'
                                className={styles.customInput}
                                onChange={(event) => {
                                    const file = event.currentTarget.files[0];
                                    setImage(file);
                                }}
                            />
                            {/* <Field name='photo'>
                                {({ form }) => (
                                    <Box
                                        sx={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}
                                    >
                                        <Box
                                            sx={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                justifyContent: 'center',
                                                columnGap: '50px'
                                            }}
                                        >

                                            <Button
                                                sx={{
                                                width: '83px',
                                                height: '34px',
                                                color: 'text.main',
                                                backgroundColor: '#F8F8F8',
                                                border: '1px solid #D0CFCF',
                                                }}
                                                onClick={handleOpenWidget}
                                            >
                                                Upload
                                            </Button>
                                            <CardMedia
                                                component='img'
                                                image={imageUrl ? imageUrl : '/photoCover.svg'}
                                                sx={{
                                                width: '70px',
                                                height: '70px',
                                                borderRadius: '50%',
                                                }}
                                            />
                                        </Box>
                                        {!imageUrl  && (
                                            <Typography variant="caption" color="error">
                                                Image is required
                                            </Typography>
                                        )}
                                    </Box>
                                )}
                            </Field> */}
                            <Button disabled={!isValid && !image} type='submit'>Sign up</Button>
                        </Box>
                    </Form>
                )}
            </Formik>
        </Container>
    );
};

export default PostRequest;
