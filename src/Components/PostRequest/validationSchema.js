import { object, string } from 'yup';

const validationSchema = object({
    name: string().required('Name is required'),
    email: string().email('Invalid email').required('Email is required'),
    phone: string().matches(/^\+380([0-9]{9})$/, 'Phone number should start on +380 and have 9 characters after').required('Phone is required'),
});

export default validationSchema;