export const container = {
  marginTop: "140px",
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
};

export const userBox = {
  display: "grid",
  alignItems: "center",
  justifyItems: "center",
  gridTemplateColumns: {
    mobile: "1fr",
    tablet: "repeat(2, 1fr)",
    desktop: "repeat(3, 1fr)",
  },
  gap: {
    mobile: "20px",
    tablet: "16px",
    desktop: "29px",
  },
  width: "100%",
  marginY: "50px",
};

export const userCard = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  justifyContent: "center",
  rowGap: "20px",
  padding: "20px",
  width: {
    mobile: "288px",
    tablet: "304px",
    desktop: "242px",
    lgDesktop: "330px",
  },
  backgroundColor: "rgba(255, 255, 255, 1)",
  borderRadius: "10px",
};
