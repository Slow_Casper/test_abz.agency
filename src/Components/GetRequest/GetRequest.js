import { Box, Button, CardMedia, Container, Typography } from '@mui/material';
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { fetchUsers } from '../../redux/slices/usersSlice';
import { container, userBox, userCard } from './styles';

const GetRequest = () => {
    // const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    const users = useSelector((state) => state.users.users);
    const nextUrl = useSelector((state) => state.users.nextUrl);
    const loading = useSelector((state) => state.users.loading);

    // const fetchUsers = async (url) => {
    //     setLoading(true)
    //     const defaultUrl = 'https://frontend-test-assignment-api.abz.agency/api/v1/users?page=1&count=6';
    //     axios(url || defaultUrl).then(res => {
    //         const { data } = res;
    //         dispatch(setUsers(data.users));
    //         dispatch(setPage(data.links.next_url));
    //         setLoading(false)
    //     });

    // };

    useEffect(() => {
        dispatch(fetchUsers())
    }, [dispatch])

    const handleClick = () => {
        dispatch(fetchUsers(nextUrl))
    };

  return (
    <Container sx={container} id='getRequest'>
        <Typography sx={{ textAlign: 'center' }} variant='h1'>Working with GET request</Typography>
        <Box sx={userBox} >
            {users && (
                users.map((el) => (
                    <Box
                        key={el.id}
                        sx={userCard}
                    >
                        <CardMedia
                            component='img'
                            image={el.photo}
                            sx={{
                                width: '70px',
                                height: '70px',
                                borderRadius: '50%',
                            }}
                        />
                        <Typography sx={{ textAlign: 'center' }}>{el.name}</Typography>
                        <Box>
                            <Typography sx={{ textAlign: 'center' }}>{el.position}</Typography>
                            <Typography sx={{ textAlign: 'center' }}>{el.email}</Typography>
                            <Typography sx={{ textAlign: 'center' }}>{el.phone}</Typography>
                        </Box>
                    </Box>
                ))
            )}
            {loading && <Typography>...Loading</Typography>}
        </Box>
        {nextUrl && <Button onClick={handleClick} sx={{width: '120px', height: '34px', marginBottom: '140px',}}>Show more</Button>}
    </Container>
  )
}

export default GetRequest