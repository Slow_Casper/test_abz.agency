import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { setError, setLoading } from '../extraReducersHelper';

export const fetchUsers = createAsyncThunk(
    'users/getUsers',
    async (url, { rejectWithValue }) => {
        const defaultUrl = 'https://frontend-test-assignment-api.abz.agency/api/v1/users?page=1&count=6';
        try {
            const { data } = await axios(url || defaultUrl);
            return data;
        } catch (err) {
            return rejectWithValue(err.response.data);
        }
    },
  );

const initialState = {
    users: [],
    nextUrl: '',
    loading: false,
    error: null,
};

const userSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    resetUsers(state, action) {
        state.users = [];
    },
  },
  extraReducers: (builder) => {
    builder
        .addCase(fetchUsers.pending, setLoading)
        .addCase(fetchUsers.fulfilled, (state, action) => {
            const { users, links } = action.payload;
            users.forEach(newUser => {
                    const existingUser = state.users.find(user => (
                        user.id === newUser.id
                    ));
                if (!existingUser) {
                    state.users.push(newUser);
                    state.nextUrl = links.next_url;
                    state.loading = false;
                }
            });
        })
        .addCase(fetchUsers.rejected, setError)
  }
});

export const { resetUsers } = userSlice.actions;

export default userSlice.reducer;
