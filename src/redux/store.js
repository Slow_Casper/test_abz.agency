import { configureStore, combineReducers } from '@reduxjs/toolkit';
import usersSlice from './slices/usersSlice';

const reducer = combineReducers({
    users: usersSlice,
});

const store = configureStore({
  reducer,
});

export default store;
