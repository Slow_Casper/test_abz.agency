import Features from "./Components/Features/Features";
import GetRequest from "./Components/GetRequest/GetRequest";
import Header from "./Components/Header/Header";
import PostRequest from "./Components/PostRequest/PostRequest";

import './styles.css';

function App() {
  return (
    <div className="app">
      <Header />
      <Features />
      <GetRequest />
      <PostRequest />
    </div>
  );
}

export default App;
