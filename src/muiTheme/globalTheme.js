import { createTheme } from '@mui/material/styles';

const globalTheme = createTheme({
  breakpoints: {
    values: {
        mobile: 0,
        tablet: 768,
        desktop: 1024,
        lgDesktop: 2560,
    },
  },
  palette: {
    text: {
        main: 'rgba(0, 0, 0, 0.87)',
        secondary: 'rgba(255, 255, 255, 1)',
    },
    background: {
        default: 'rgba(248, 248, 248, 1)',
    },
  },
  shape: {
    borderRadius: 80,
  },
  typography: {
    fontFamily: 'Nunito',
    fontWeightSemiBold: 400,
  },
  components: {
    MuiButton: {
      defaultProps: {
        disableRipple: true,
      },
      styleOverrides: {
        root: {
          color: 'rgba(0, 0, 0, 0.87)',
          backgroundColor: 'rgba(244, 224, 65, 1)',
          width: '100px',
          height: '34px',
          '&: hover': {
            backgroundColor: 'rgba(255, 227, 2, 1)',
          },
          '&:disabled': {
            backgroundColor: 'rgba(180, 180, 180, 1)',
            color: 'rgba(255, 255, 255, 0.87)',
          }
        },
      },
    },
    MuiFormLabel: {
      styleOverrides: {
        root: {
          color: 'rgba(126, 126, 126, 1)',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        root: {
          fieldset: {
            border: '1px solid #6C5FBC',
            borderRadius: 4,
          },
          input: {
            '&::placeholder': {
              color: 'rgba(0, 0, 0, 0.87)',
            },
          },
          textarea: {
            '&::placeholder': {
              color: 'rgba(0, 0, 0, 0.87)',
            },
          },
        },
      },
    },
    MuiContainer: {
      styleOverrides: {
        root: ({ theme }) => ({
          [theme.breakpoints.up('mobile')]: {
            paddingRight: '16px',
            paddingLeft: '16px',
          },
          [theme.breakpoints.up('tablet')]: {
            paddingRight: '65px',
            paddingLeft: '65px',
          },
          [theme.breakpoints.up('desktop')]: {
            paddingRight: '90px',
            paddingLeft: '90px',
          },
          maxWidth: '1170px',
          minWidth: '328px',
        }),
      },
    },
    MuiRadioColorPrimary: {
      styleOverrides: {
        root: {
          color: 'rgba(0, 189, 211, 1)',
        },
      },
    },
  },
});

globalTheme.typography.body1 = {
  fontFamily: 'Nunito',
  fontWeight: 400,
  lineHeight: '26px',
  fontSize: '16px',
};

globalTheme.typography.h1 = {
  fontFamily: 'Nunito',
  fontWeight: 400,
  lineHeight: '40px',
  fontSize: '40px',
};

export default globalTheme;
